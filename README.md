# TVDB API démo
You are going to learn how to use the REST API of the https://www.thetvdb.com site.

## Discover and explore the API
You can read the documentation here : https://api.thetvdb.com/swagger#/

It's a good start point :)

Then we propose with this exercise to use this API with different clients.

Do you really know everything on the Queen's Gambit series ? Let's check that with the API ! 

### Use of the new HTTP client appeared with Java 9 and marked as stable in Java 11
Some resources about this new HTTP client :
* https://docs.oracle.com/en/java/javase/11/docs/api/java.net.http/java/net/http/HttpRequest.html
* https://docs.oracle.com/en/java/javase/11/docs/api/java.net.http/java/net/http/HttpClient.html
* https://www.baeldung.com/java-9-http-client
* https://mkyong.com/java/java-11-httpclient-examples/

### Use of the WebClient of Spring
Some resources about it :
* https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/reactive/function/client/WebClient.html
* https://docs.spring.io/spring-boot/docs/2.0.3.RELEASE/reference/html/boot-features-webclient.html
* https://www.baeldung.com/spring-5-webclient

### Use of a library which does all the work for us
We propose to use the MovieJukebox TheTvDbApi library.
There's just some mapping to do :)