/**
 * 
 */
package org.hbs.TvdbAPIDemo;

import java.io.IOException;
import java.util.List;

import org.hbs.TvdbAPIDemo.model.Episode;
import org.hbs.TvdbAPIDemo.model.TvShow;

/**
 * @author Halima
 *
 */
public interface ITvdbDownloader {
	
	/**
	 * @return
	 * @throws IOException
	 */
	public boolean login() throws IOException ; 
	
	/**
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public List<TvShow> searchShow(String name) throws IOException; 
	
	/**
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public List<Episode> getEpisodes(int id) throws IOException; 
}
