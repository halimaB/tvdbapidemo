package org.hbs.TvdbAPIDemo.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.http.HttpResponse;

/**
 * @author Halima
 *
 */
public class TvshowParser {

	public static <T> T getObjectFromJson(HttpResponse<String> response, Class<T> target) throws JsonProcessingException {
		var objectMapper = new ObjectMapper();
		return objectMapper.readValue(response.body(), target);
	}
}
