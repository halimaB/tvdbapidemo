package org.hbs.TvdbAPIDemo;

/**
 * @author Halima
 *
 */
public class Constants {
	public final static String THETVDB_API_URL = "https://api.thetvdb.com";
	public final static String THETVDB_API_LOGIN = "/login";
	public final static String THETVDB_API_SEARCH = "/search/series";
	public final static String THETVDB_API_EPISODE = "/series/ID/episodes";

	public final static String THETVDB_API_KEY = "apikey";
	public final static String THETVDB_API_SERIES_NAME = "name";

	public final static String THETVDB_API_KEY_VALUE = "YOUR_API_KEY";

}
