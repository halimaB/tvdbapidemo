package org.hbs.TvdbAPIDemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.hbs.TvdbAPIDemo.model.Episode;
import org.hbs.TvdbAPIDemo.model.TvShow;
import org.junit.jupiter.api.Test;

/**
 * @author Halima
 *
 */
class ITvdbDownloaderTest {

	@Test
	void testJava11Downloader() throws IOException {
		ITvdbDownloader downloader = new Java11TvdbDownloaderImpl() ;
		testDownloader(downloader);
	}

	@Test
	void testSpringRestDownloader() throws IOException {
		ITvdbDownloader downloader = new SpringRestTvdbDownloaderImpl() ;
		testDownloader(downloader);
	}
	
	@Test
	void testMjbLibTvdbDownloader() throws IOException {
		ITvdbDownloader downloader = new MjbLibTvdbDownloaderImpl() ;
		testDownloader(downloader);
	}
	
	
	private void testDownloader(ITvdbDownloader downloader) throws IOException {
		assertTrue(downloader.login()) ;
		List<TvShow> tvShows = downloader.searchShow("queen gambit") ;
		assertEquals(tvShows.size(),2) ;
		TvShow queenGambit = tvShows.get(0);
		assertEquals(queenGambit.getSeriesName(), "The Queen's Gambit") ;
		List<Episode> episodes = downloader.getEpisodes(queenGambit.getId()) ;
		assertEquals(episodes.size(), 7) ;
		assertEquals(episodes.get(0).getEpisodeName(), "Openings") ;
	}
}
